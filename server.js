var express = require('express'),
  _ = require('lodash'),
  cors = require('cors'),
  bodyparser = require('body-parser'),
  app = express(),
  storage = [{
    id: _.uniqueId(),
    fromAccountId: 1,
    toAccountId: 2,
    text: 'test',
    receivedAt: '' + new Date(),
    unread: true
  }];

app.use(cors());
app.use(bodyparser());

app.get('/messages/:id/inbox', function (req, res, next) {

  var myMessages = _.filter(storage, function (message) {
    return message.toAccountId === +req.params.id
  });

  console.log(storage, myMessages, req.params);

  res.json(_.map(myMessages, function (message) {
    return {
      id: message.id,
      fromAccountId: message.fromAccountId,
      text: message.text,
      receivedAt: message.date,
      unread: message.unread
    };
  }));
});

app.get('/messages/:id/sent', function (req, res, next) {
  var myMessages = _.filter(storage, function (message) {
    return message.fromAccountId === +req.params.id
  });
  res.json(myMessages);
});

app.get('/messages/message/:messageId', function (req, res, next) {
  var message = _.find(storage, function (msg) {
    return msg.id === req.params.messageId;
  });

  if (message === undefined) {
    res.json({});
  } else {
    message.unread = false;
    res.json(message);
  }
});

app.post('/messages/:id', function (req, res, next) {
  if (req.body.message === undefined || req.body.message.toAccountId === undefined || req.body.message.text === undefined) {
    res.json(false);
    return;
  }

  var message = req.body.message;
  message.id = _.uniqueId();
  message.fromAccountId = +req.params.id;
  message.toAccountId = +message.toAccountId;
  message.receivedAt = '' + new Date(),
  message.unread = true,

  storage.push(message);

  res.json(true);
});

var port = process.env.PORT || 3000;
app.listen(port, function () {
  console.log('CORS-enabled web server listening on port ' + port);
});

module.exports = app;
