var request = require('supertest')
  , express = require('express')
  , app = require('../server');

describe("express app", () => {

  it("can get message", (done) => {
    request(app)
      .get('/messages/1/inbox')
      .expect('Content-Type', /json/)
      .expect(200, done);
  });

  it("can post message", (done) => {
    request(app)
      .post('/messages/1')
      .send('{"message": {"text": false}}')
      .set('Accept', 'application/json')
      .expect(200, done);
  });

  it("fails for missing routes", (done) => {
    request(app)
      .get('/messa')
      .expect(404, done);
  });
});

