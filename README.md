This is the Messages service of Dare2Date.

To call it, you need to know the Account ID.

Listing the inbox
=================

```
// GET https://messages.dare2date.com/messages/1234/inbox
```

```json
{
    "messages": [
        {
            "id": 14450,
            "fromAccountId": 69,
            "text": "Hello cowboy",
            "receivedAt": "2015-06-05T14:00:13Z",
            "unread": false
        },
    ],
}
```
"id" is a unique message ID.

"fromAccountId" is the account ID of the sender. To get further details about the sender, call into the Profiles service.

"receivedAt" is an ISO 8601 timestamp in UTC.


Sending a message
=================

```
POST https://messages.dare2date.com/messages/1234
```

```json
{
    "message": {
        "toAccountId": 14450,
        "text": "Hello doll"
    }
}
```

Planned features
================
* Marking a message as read or unread
* Support for multiple folders
* ...
*

